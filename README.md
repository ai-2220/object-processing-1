# ПЗ1 по обработке изображений

## Состав репозитория

В [static](static/) хранятся изображения, видосы и гифки.
В [scripts](scripts/) хранятся код и блокноты

## :sparkles: Задание на плюсик

В качестве ресурса для motion detection использовал видео с YouTube и библиотеку [pytube](https://pytube.io/en/latest/).

### Алгоритм работы скрипта

Помещаю первый фрейм в переменную first_frame и когда данный массив заполнен, начинается основная работа скрипта.
Нахожу абсолютную разницу между первый и вторым кадром (frame - второй кадр)

```
d = cv2.absdiff(first_frame, frame)
```

Перевожу изображение в серый

```
grey = cv2.cvtColor(d, cv2.COLOR_BGR2GRAY)
```

Размываю выходное изображение

```
blur = cv2.GaussianBlur(grey, (15, 15), 0)
```

Убираю пороговые значения

```
th = cv2.threshold(blur, 25, 255, cv2.THRESH_BINARY)[1]
```

Расширяю белые регионы

```
dilated = cv2.dilate(th, None, iterations=2)
```

Дальше ищу контура и ищу bbox

Сохраняю видео в формат mp4

![Spider video](static/test-with-tracker.gif)

## :zzz: Плановое задание по созданию фильтра из Instagram* 
* -- Meta Platforms Inc. признана экстремистской организацией на территории РФ

```
cd ./scripts/FilterCarti

dotnet run
```

### Алгоритм работы скрипта

Получаю исходные изображения, где baseImage - базовое изображение пользователя, redImage - изображение текста, накладываемого поверх фото

```
Image<Bgr, byte> baseImage = new Image<Bgr, byte>($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\22.jpg");
Image<Bgr, byte> redImage = new Image<Bgr, byte>($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\RED.png");
```

Изменяю размер базового изображения и увеличиваю яркость

```
var brightnessImage = Increasingbrightness(resizedBasedImage, 40);
```

Перевожу изображение в серый

```
Image<Gray, byte> grayImage = brightnessImage.Convert<Gray, byte>();
```

Убираю пороговые значения

```
grayImage = grayImage.ThresholdBinary(new Gray(150), new Gray(255));
```

Сглаживаю контуры серого изображения

```
CvInvoke.MedianBlur(imagePlayboiCarti, imagePlayboiCarti, 5);
```

Вычисляю начальные координаты для наложения текста на получившееся изображение

```
int startX = imagePlayboiCarti.Width / 2 - redImage.Width / 2;
int startY = imagePlayboiCarti.Height / 25;
```

Применяю фильтр и сохраняю результат 

```
var resultImage = PlayboiCartiFilter(imagePlayboiCarti, redImage, startX, startY);
CvInvoke.Imwrite($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\carti-dima.png", resultImage);
```

Выходное изображение (обложка Playboi Carti)

<img src="static/carti-dima.png" height="250" />
<img src="static/PlayboiCarti.jpeg" height="250"/>