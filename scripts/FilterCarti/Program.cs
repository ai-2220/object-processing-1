﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace FilterApp;

public class Program
{
    private static void Main()
    {
        Image<Bgr, byte> baseImage = new Image<Bgr, byte>($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\22.jpg");
        Image<Bgr, byte> redImage = new Image<Bgr, byte>($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\RED.png");

        Image<Bgr, byte> resizedBasedImage = baseImage.Resize(baseImage.Width / 3, baseImage.Height / 3, Inter.Linear);

        var brightnessImage = Increasingbrightness(resizedBasedImage, 40);

        Image<Gray, byte> grayImage = brightnessImage.Convert<Gray, byte>();
        grayImage = grayImage.ThresholdBinary(new Gray(150), new Gray(255));
        var imagePlayboiCarti = grayImage.Convert<Bgr, byte>();

        CvInvoke.MedianBlur(imagePlayboiCarti, imagePlayboiCarti, 5);

        int startX = imagePlayboiCarti.Width / 2 - redImage.Width / 2;
        int startY = imagePlayboiCarti.Height / 25;


        var resultImage = PlayboiCartiFilter(imagePlayboiCarti, redImage, startX, startY);

        CvInvoke.Imwrite($"{Environment.CurrentDirectory.Split("\\bin")[0]}\\images\\carti-dima.png", resultImage);
        CvInvoke.Imshow("Playboy Carti style", resultImage);
        CvInvoke.WaitKey(0);
    }

    private static Image<Bgr, byte> PlayboiCartiFilter(Image<Bgr, byte> basedImage, Image<Bgr, byte> textImage, int startX, int startY)
    {
        CvInvoke.MedianBlur(textImage, textImage, 7);

        for (int y = 0; y < textImage.Height; y++)
        {
            for (int x = 0; x < textImage.Width; x++)
            {
                Bgr pixelRed = textImage[y, x];

                if (startX + x < basedImage.Width && startY + y < basedImage.Height)
                {
                    Bgr pixelBased = basedImage[startY + y, startX + x];

                    if (!IsPixelBlackOrNotRed(pixelRed))
                    {
                        pixelBased = pixelRed;
                    }

                    basedImage[startY + y, startX + x] = pixelBased;
                }
            }
        }
        return basedImage;

    }

    private static Image<Bgr, byte> Increasingbrightness(Image<Bgr, byte> resizedBasedImage, int brightnessIncrease)
    {
        for (int y = 0; y < resizedBasedImage.Height; y++)
        {
            for (int x = 0; x < resizedBasedImage.Width; x++)
            {
                Bgr pixel = resizedBasedImage[y, x];

                pixel.Blue = (byte)Math.Min(255, pixel.Blue + brightnessIncrease);
                pixel.Green = (byte)Math.Min(255, pixel.Green + brightnessIncrease);
                pixel.Red = (byte)Math.Min(255, pixel.Red + brightnessIncrease);

                resizedBasedImage[y, x] = pixel;
            }
        }
        return resizedBasedImage;
    }

    private static bool IsPixelBlackOrNotRed(Bgr pixel)
    {
        return (pixel.Blue == 0 && pixel.Green == 0 && pixel.Red == 0) || !(pixel.Blue != 10 || pixel.Green != 10 || pixel.Red != 210);
    }
}